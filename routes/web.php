<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Http\Request;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'],function(){

    //Rutas para Categorias
    Route::resource('categories','CategoriesController', ['parameters' => [
        'categories' => 'id'
    ]]);

    //Rutas para Productos
    Route::resource('products','ProductsController', ['parameters' => [
        'products' => 'id'
    ]]);
});

//Grupo de rutas para las api rest
Route::group(['prefix' => 'api'],function(){
    Route::get('categories/list_categories', 'CategoriesController@listado')->name('list_categories');
    Route::get('products/list_products', 'ProductsController@listado')->name('list_products');
    Route::post('/upload', 'ProductsController@upload_files')->name('upload_files');
});

