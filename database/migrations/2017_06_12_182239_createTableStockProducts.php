<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStockProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('stockproducts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_action')->comment('Tipo 1: SUMA, Tipo 2: RESTA');
            $table->decimal('cant_action',10,2);
            $table->string('action_comment');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('stockproducts');
    }
}
