<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Categorie;
use App\Imgproduct;

class ProductsController extends Controller
{
    //Constructor
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = Product::find(1);
        return view('admin.products.index',['list' => $list]);
    }

    public function listado()
    {
        //
        $data = Product::all();
        $i=0;
        foreach ($data as $value) {
            $data[$i]['categorie_name'] = $value->categorie->name;
            $i++;
        }
        return compact('data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $items = Categorie::pluck('name','id');
        return view('admin.products.create',['list' => $items]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product($request->all());
        $product->status = ($request->status)?1:0;
    	$product->save();
        // flash('Se ha registrado un nuevo usuario exitosamente!')->success();
    	return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $items = Categorie::pluck('name','id');
        return view('admin.products.edit',['product' => $product,'list' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->status = ($request->status)?1:0;
        $product->categorie_id = $request->categorie_id;
        $product->save();
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response()->json(['data' => ['status' => 'ok','producto' => $product,'msg' => 'Producto eliminado']], 200);
    }

    public function upload_files(Request $request){

        $validator = \Validator::make($request->all(), [
            // 'image' => 'required|image64:jpeg,jpg,png'
        ]);
        if ($validator->fails()) {
            return response()->json(['status'=>false,'errors'=>$validator->errors()]);
        } else {
            $imageData = $request->get('image');
            $idProduct = $request->get('id');
            $fileName = \Carbon\Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('photos/upload/').$fileName);
            
            //Guardamos el registro de la imagen
            $productImg = new Imgproduct();
            $productImg->image_url = 'photos/upload/'.$fileName;
            $productImg->product_id = $idProduct;
            $productImg->save();

            return response()->json(['status'=>true,'data' => $productImg]);
        }
    }
}
