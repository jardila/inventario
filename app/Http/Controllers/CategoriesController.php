<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorie;

class CategoriesController extends Controller
{

    //Constructor
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $list = Categorie::all();
        
        return view('admin.categories.index',['list' => $list]);
    }

    public function listado()
    {
        //
        $data = Categorie::all();

        // return view('admin.categories.index',['list' => $data]);
        return compact('data');
        // return response()->json(['body' => $body], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $categorie = new Categorie($request->all());

    	$categorie->save();
        //flash('Se ha registrado un nuevo usuario exitosamente!')->success();
    	return response()->json(['data' => ['status' => 'ok','categorie' => $categorie,'msg' => 'Categoria agregada']], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $categorie = Categorie::find($id);
        $categorie->name = $request->name;
        $categorie->description = $request->description;
        $categorie->status = $request->status;
        $categorie->color = $request->color;
        $categorie->save();
        return response()->json(['data' => ['status' => 'ok','categorie' => $categorie,'msg' => 'Categoria actualizada']], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorie = Categorie::find($id);
        $categorie->delete();
        return response()->json(['data' => ['status' => 'ok','categorie' => $categorie,'msg' => 'Categoria eliminada']], 200);
    }
}
