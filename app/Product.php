<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //

    protected $table = 'products';

    protected $fillable = [
        'name',
        'description',
        'status',
        'price',
        'categorie_id',
    ];



    public function categorie(){
        return $this->belongsTo('App\Categorie');
    }

    public function imgproduct(){
        return $this->hasMany('App\Imgproduct');
    }
}
