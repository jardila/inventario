<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    //
    protected $table = 'categories';

    protected $fillable = ['name','description','status','color'];

    // public function product()
    // {
    //     return $this->belongsToMany('App\Product');
    // }
}
