<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imgproduct extends Model
{
    //
    protected $table = 'imgproducts';

    protected $fillable = [
        'image_url',
        'product_id'
    ];

    // public function product()
    // {
    //     return $this->belongsTo('App\Product');
    // }
}
