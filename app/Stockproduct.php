<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stockproduct extends Model
{
    //
    protected $table = 'stockproducts';

    protected $fillable = [
        'type_action',
        'cant_action',
        'comment_action',
        'product_id'
    ];
}
