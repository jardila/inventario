import * as axios from 'axios';

const BASE_URL = 'http://localhost/web_roy/public';

function upload(formData) {
    const url = BASE_URL+'/photos/upload';
    console.log(url);
    return axios.post(url, formData)
        // get data
        .then(x => x.data)
        // add url field
        .then(x => x.map(img => Object.assign({},
            img, { url: `${BASE_URL}/images/${img.id}` })));
}

export { upload }