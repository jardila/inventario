@extends('layouts.app')

@section('content')


        <smart-table 
            :fields="[ 
                {'title': 'Id', 'type':'text','name':'id','pk':'true'},
                {'title': 'Name', 'type':'text','name':'name'},
                {'title': 'Descripcion', 'type':'text','name':'description'},
                {'title': 'Precio', 'type':'text','name':'price'},
                {'title': 'Categoria', 'type':'text','name':'categorie_name'},
                {'title': 'Estado', 'type':'status','name':'status'}
                ]"
            :model="{'plural': 'Productos', 'singular': 'Producto'}"
            :show-tfoot="false"
            :has-modal="false"
            :url="{
                'create':'products.create',
                'edit':'products.edit',
                'store':'products.store',
                'update':'products.update',
                'destroy':'products.destroy',
                'index': 'products.index',
                'list': 'list_products'
                }"></smart-table>
@endsection
