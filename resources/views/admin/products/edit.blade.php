@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::model($product,['route' => ['products.update',$product->id], 'method' => 'PUT', 'role' => 'form','class' => 'form-horizontal']) !!}
<panel-global title="Editar Producto" type="success" large="12">
    <div slot="body">
        <div class="col-sm-12">
            <div class="row">                
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('name','Nombre:',array('class' => 'col-sm-4 control-label')) !!}
                        <div class="col-sm-8">
                        {!! Form::text('name',null,['class' => 'form-control','placeholder' => 'Tu Nombre','required' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Descripcion:',array('class' => 'col-sm-4 control-label')) !!}
                        <div class="col-sm-8">
                        {!! Form::text('description',null,['class' => 'form-control','placeholder' => 'Descripcion','required' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('price','Precio:',array('class' => 'col-sm-4 control-label')) !!}
                        <div class="col-sm-8">
                        {!! Form::text('price',null,['class' => 'form-control','placeholder' => '$0,00','required' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('categorie_id','Categoria:',array('class' => 'col-sm-4 control-label')) !!}
                        <div class="col-sm-8">
                        {!! Form::select('categorie_id',$list,null,['class' => 'form-control','required' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('status','Estatus:',array('class' => 'col-sm-4 control-label')) !!}
                        <div class="col-sm-8">
                        {!! Form::checkbox('status') !!}
                        </div>
                    </div>
                </div>
            </div>
            <h3>Imagenes del producto </h3>
            <hr>
            <div class="row">
                <div class="col-lg-10">
                    <file-upload id-product="{!! $product->id !!}"></file-upload>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-lg-12">
                @foreach ($product->imgproduct as $imagen)  
                    <div class="img-productos">
                    <img src="{{ asset($imagen->image_url) }}" class="img img-responsive">
                    </div>
                @endforeach  
                </div>
            </div>
        </div>
    </div>

    <div slot="footer">
        <div class="row">                
            <div class="col-lg-6">
                {!! Form::submit('Editar',['class' => 'btn btn-success']) !!}
                {!! link_to_route('products.index','Cancelar', $parameters = [], $attributes = ['class' => 'btn btn-default']); !!}
            </div>
        </div>
    </div>
</panel-global>
{!! Form::close() !!}
@endsection

@section('scripts')
<style>
    .img-productos{
        float:left;
        width:200px;
        margin-right: 15px;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('#status').bootstrapSwitch();
    });
</script>
@stop