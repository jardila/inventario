@extends('layouts.app')

@section('content')

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{!! Form::open(['route' => 'products.store', 'method' => 'POST', 'role' => 'form']) !!}
<panel-global title="Crear Producto Nuevo" type="success" large="12">
    <div slot="body">
        <div class="col-sm-12">
            <div class="row">                
                <div class="col-lg-6">
                    <div class="form-group">
                        {!! Form::label('name','Nombre:') !!}
                        {!! Form::text('name',null,['class' => 'form-control','placeholder' => 'Nombre del Producto','required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('description','Descripcion:') !!}
                        {!! Form::text('description',null,['class' => 'form-control','placeholder' => 'Descripcion','required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('price','Precio:') !!}
                        {!! Form::text('price',null,['class' => 'form-control','placeholder' => '$0,00','required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('categorie_id','Categoria:') !!}
                        {!! Form::select('categorie_id',$list,null,['class' => 'form-control','required' => true]) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('status','Estatus:') !!}
                        {!! Form::checkbox('status') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div slot="footer">
        <div class="form-group">
            {!! Form::submit('Guardar',['class' => 'btn btn-success']) !!}
            {!! link_to_route('products.index','Cancelar', $parameters = [], $attributes = ['class' => 'btn btn-default']); !!}
        </div>
    </div>
</panel-global>
{!! Form::close() !!}
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#status').bootstrapSwitch();
    });
</script>
@stop