@extends('layouts.app')

@section('content')


        <smart-table 
            :fields="[ 
                {'title': 'Id', 'type':'text','name':'id','pk':'true'},
                {'title': 'Name', 'type':'text','name':'name'},
                {'title': 'Descripcion', 'type':'text','name':'description'},
                {'title': 'Color', 'type':'color','name':'color'},
                {'title': 'Estado', 'type':'status','name':'status'}
                ]"
            :model="{'plural': 'Categories', 'singular': 'Categorie'}"
            :show-tfoot="false"
            :has-modal="true"
            :url="{
                'create':'categories.create',
                'edit':'categories.edit',
                'store':'categories.store',
                'update':'categories.update',
                'destroy':'categories.destroy',
                'index': 'categories.index',
                'list': 'list_categories'
                }"></smart-table>
@endsection
